const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const uglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        'widget': './src/modules/widget/webcomponent/webcomponent.js',
        'builder': './src/modules/builder/webcomponent/webcomponent.js',
        'styling': './src/modules/styling/webcomponent/webcomponent.js'
    },
    output: {
        // library: libraryName,
        // libraryTarget: 'umd',
        // libraryExport: 'default',
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 200000000, // Convert images < 8kb to base64 strings
                        name: 'img/[hash]-[name].[ext]',
                    },
                }],
            },

            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
        ],
    },
    plugins: [
        new uglifyJsPlugin(),
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, 'index.html'),
        }),
        new CopyPlugin({
            patterns: [
                {
                    context: './node_modules/tinymce/skins/',
                    from: './**/*',
                    to: './skins',
                },
                {
                    context: './node_modules/tinymce/themes/',
                    from: './**/*',
                    to: './themes',
                }]
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
};