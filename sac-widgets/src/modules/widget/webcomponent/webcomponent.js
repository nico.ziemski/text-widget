import styles from '../scss/style.scss';
import template from '../html/template.html';
import * as d3 from '../lib/index.js';
import { yScale, xScale, xLineScale } from '../lib/charts';
import { setElementReadOnly, removeElementReadOnly, hideElement, showElement } from '../lib/dom';
import { calculateChangeInPercentage, calculateChange } from '../lib/calc';
import { format, Scaling, getScaling, getRange } from '../lib/format';
import { DEFAULT_SETTINGS } from '../../config/defaults';
import { selection } from 'd3';
const templateDom = document.createElement('template');

templateDom.innerHTML = `
  <style>${styles.toString()}</style>
  ${template.toString()}`;



class WidgetWebComponent extends HTMLElement {

    get titleControl() {
        return this.shadowDOM.getElementById('sac-widget-name');
    }

    get subtitleControl() {
        return this.shadowDOM.getElementById('sac-widget-name-back');
    }

    getKpiDescriptionControl(index) {
        return this.shadowDOM.getElementById('kpi-' + index + '-desc');
    }

    get kpiDescriptionControls() {
        return this.shadowDOM.querySelectorAll('input.desc');
    }

    get comparisonValueControls() {
        return this.shadowDOM.querySelectorAll('input.comp-value-desc');
    }

    get kpiValuesControls() {
        return this.shadowDOM.querySelectorAll('span.value');
    }

    set title(value) {
        this.titleControl.value = value;
        this.subtitleControl.innerText = value;
    }

    set data(value) {
        this._data = value;
        this.removeSvgElement();
        this.createSvgElement();
        this.createChart();
    }

    set chartTitle(value) {
        this._chartTitle = value;
    }

    get chartTitle() {
        return this._chartTitle;
    }

    set lineData(value) {
        this._lineData = value;
        this.removeSvgElement();
        this.createSvgElement();
        this.createChart();
    }

    set kpi1Description(value) {
        this.getKpiDescriptionControl(1).value = value;
    }

    set kpi2Description(value) {
        this.getKpiDescriptionControl(2).value = value;
    }

    set kpi3Description(value) {
        this.getKpiDescriptionControl(3).value = value;
    }

    set kpi4Description(value) {
        this.getKpiDescriptionControl(4).value = value;
    }

    get size() {
        return this.shadowDOM.querySelector('.widget-container').getBoundingClientRect();
    }

    get inputControls() {
        return this.shadowDOM.querySelectorAll('input');
    }

    get rootControl() {
        return this.shadowDOM.querySelector('.widget-container');
    }

    get comparisonValuesModel() {
        const kpis = Array.from(this.shadowDOM.querySelectorAll('.compare-values-container'));

        const values = kpis.map(container => {
            const descriptions = Array.from(container.querySelectorAll('input.comp-value-desc'));
            return descriptions.map(input => input.value)
        });

        return JSON.stringify({ comparisonValues: values });
    }

    set designMode(value) {
        if (value) {
            this.inputControls.forEach(control => {
                removeElementReadOnly(control)
            });
            this.shadowDOM.querySelector('.widget-container').classList.remove('runtime');
        } else {
            this.inputControls.forEach(control => {
                setElementReadOnly(control)
            });
            this.shadowDOM.querySelector('.widget-container').classList.add('runtime')
        }

        if (!value) {
            this.disableKpis();
            this.disableComparisonValues();
        }

        this._designMode = value;
    }

    get designMode() {
        return this._designMode;
    }

    set _comparisonValues(value) {
        console.log('_comparisonValues setter', value);
        if (!value) return;

        const model = JSON.parse(value);

        if (model.comparisonValues) {

            const kpis = Array.from(this.shadowDOM.querySelectorAll('.compare-values-container'));

            kpis.forEach((container, index) => {
                const descriptions = Array.from(container.querySelectorAll('input.comp-value-desc'));
                const values = model.comparisonValues[index];

                descriptions.forEach((input, iindex) => {
                    input.value = values[iindex];
                });

            });

            // model.comparisonValues.forEach(kpi => {
            //     kpi.forEach(comparisonValue => {

            //     })
            // });
        }
    }

    set description(value) {
        this.shadowDOM.querySelector('p.description').innerHTML = value;
    }

    set css(value) {
        if (typeof value !== 'undefined') {
            this.styleNode.innerHTML = value;
            this.shadowDOM.appendChild(this.styleNode.cloneNode(true));
        }
    }

    set _config(value) {
        let config = {};

        if (!value) return;

        try {
            config = JSON.parse(value);
            this.applyConfig(config);
            this.disableKpis();
            this.disableComparisonValues();
        }
        catch {
            console.warn("Incorrect json format", value);
        }
    }

    /*
    You can implement this function to execute JavaScript code when the Web Component is initialized,
    for example, when the custom widget is added to the canvas or the analytic application is opened.
    This function is the counterpart of onCustomComponentDestroy.
    */

    constructor() {
        super();

        this.shadowDOM = this.attachShadow({ mode: 'open' });
        this.shadowDOM.appendChild(templateDom.content.cloneNode(true));

        this.styleNode = document.createElement('style');

        this._data = [];
        this._lineData = [];
        this._chartTitle = '';

        this._values = new Array(4);
        this._comparisonValuesArray = new Array(16);

        this._width;
        this._height;
        this._designMode;

        this.config = {};


        this.addEventListeners();

        this.applyConfig(DEFAULT_SETTINGS);
        this.createSvgElement();
        this.disableKpis();
        this.disableComparisonValues();

        this.designMode = true;
    }

    /*
    You can implement this function to execute JavaScript code when this Web Component of the custom widget is connected to the HTML DOM of the web page
    */

    connectedCallback() {

    }

    /*
    You can implement this function to execute JavaScript code when this Web Component of the custom widget is disconnected from the HTML DOM of the web page.
    */

    disconnectedCallback() {

    }

    /*
    You can implement this function to execute JavaScript code after the properties of the custom widget have been updated.
    The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
    The key is the name of the property, the value is the changed value of the property.
    When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
    The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
    */

    onCustomWidgetAfterUpdate() {

    }

    /*
    You can implement this function to execute JavaScript code before the properties of the custom widget are updated.
    The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
    The key is the name of the property, the value is the changed value of the property.
    When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
    The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
    */

    onCustomWidgetBeforeUpdate(oChangedProperties) {
        this.props = { ...this.props, ...oChangedProperties };
        this.designMode = oChangedProperties.hasOwnProperty('designMode') ? oChangedProperties.designMode : this.designMode;
    }

    /*
    You can implement this function to execute JavaScript code when the custom widget is destroyed,
    for example, when the custom widget is removed from the canvas or the analytic application containing the custom widget is closed.
    This function is the counterpart of constructor().
    */

    onCustomWidgetDestroy() {

    }

    /*
    You can implement this function to execute JavaScript code when the custom widget is resized.
    The argument width is the new width of the custom widget in pixels.
    The argument height is the new height of the custom widget in pixel
    */

    onCustomWidgetResize() {
        this._width;
        this._height;

        this.removeSvgElement();
        this.createSvgElement();
        this.createChart();
    }

    propertyChange(propertyName, value) {
        const event = {
            detail: { properties: {} }
        };

        event.detail.properties[propertyName] = value;

        this.dispatchEvent(new CustomEvent("propertiesChanged", event));
    }

    createSvgElement() {
        const lineChartEnabled = this.getConfiguration('lineChartEnabled');
        const barChartEnabled = this.getConfiguration('barChartEnabled');

        if (!lineChartEnabled && !barChartEnabled) return;

        this.svg = d3.select(this.shadowDOM.getElementById('content'))
            .append("svg")
            // .attr('viewBox', '0 0 378 200')
            .attr('width', '100%');
    }

    removeSvgElement() {
        this.removeTooltip();
        if (this.svg) {
            this.svg.remove();
        }
    }

    removeTooltip() {
        d3.select(this.shadowDOM.querySelector('.toolTip')).remove();
    }

    createChart() {

        const barChartEnabled = this.getConfiguration('barChartEnabled');
        const lineChartEnabled = this.getConfiguration('lineChartEnabled');

        const svg = this.shadowDOM.querySelector('svg');

        if (!svg) {
            return;
        }

        const svgBox = svg.getBoundingClientRect();

        const CHART_HEIGHT = svgBox.height;
        const width = svgBox.width;
        const data = this._data;
        const lineData = this._lineData;

        const scaleLineX = xLineScale(data, width);
        const scaleX = xScale(data, width);

        const scaleY = yScale([...data, ...lineData], CHART_HEIGHT);

        if (barChartEnabled) {
            this.createBarChart(data, scaleX, scaleY, CHART_HEIGHT);
        }

        if (lineChartEnabled) {
            this.createLineChart(lineData, scaleX, scaleY, scaleLineX);
        }
    }

    createLineChart(lineData, scaleX, scaleY, scaleLineX) {


        const line = d3.line()
            .x(function (d, i) { return scaleLineX(i) + (scaleX.bandwidth() / 2) }) // set the x values for the line generator
            .y(function (d) { return scaleY(d); })


        this.svg.append("path")
            .datum(lineData) // 10. Binds data to the line 
            .attr("class", "line") // Assign a class for styling 
            .attr("d", line); // 11. Calls the line generator 
    }

    createBarChart(data, scaleX, scaleY, CHART_HEIGHT) {

        const decimalPlaces = this.getConfiguration('hoverDecimalPlaces') || 2;
        const scaling = this.getConfiguration('hoverScaling');
        const formatSetting = this.config['widgetNumberFormatting'];

        const toolTip = d3.select(this.rootControl).append("div").attr("class", "toolTip");
        const tooltipText = () => this.chartTitle;
        const formatValue = (value) => format(value, formatSetting, decimalPlaces, scaling, '', true);
        const container = this.rootControl;

        const tooltipEnabled = this.getConfiguration('hoverEnabled');

        let tt = this.shadowDOM.querySelector('.toolTip');

        const chart = this.svg.selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("x", function (d, i) {
                return scaleX(i);
            })
            .attr("y", function () {
                return scaleY(0);
            })
            .attr("width", scaleX.bandwidth())
            .attr("height", function () {
                return CHART_HEIGHT - scaleY(0);
            });

        if (tooltipEnabled) {
            chart
                .on("mousemove", function (d) {
                    var rect = container.getBoundingClientRect(),
                        ox = d3.event.pageX - rect.left - this.clientLeft - window.pageXOffset - 30,
                        oy = d3.event.pageY - rect.top - this.clientTop - window.pageYOffset - 50;

                    const tooltipSize = tt.getBoundingClientRect().width;
                    const right = rect.width - ox;

                    if (right <= ((tooltipSize))) {
                        ox = ox - (tooltipSize / 2);
                    }

                    toolTip
                        .style("left", ox + "px")
                        .style("top", oy + "px")
                        .style("display", "inline-block")
                        .html('<span>' + (tooltipText()) + '</span><span>' + formatValue(d) + '</span>');
                })
                .on("mouseout", function (d) { toolTip.style("display", "none"); })
        }

        // animate bars
        this.svg.selectAll("rect")
            .transition()
            .duration(800)
            .attr("y", function (d) { return scaleY(d) })
            .attr("height", function (d) { return CHART_HEIGHT - scaleY(d); })
            .delay(function (d, i) { return (i * 100) })
    }

    addEventListeners() {
        this.titleControl.addEventListener('change', (value) => {
            this.propertyChange('title', value.target.value);
        });

        this.kpiDescriptionControls.forEach((control, index) => {
            control.addEventListener('change', (value) => {
                this.propertyChange('kpi' + (index + 1) + 'Description', value.target.value);
            });
        });

        this.comparisonValueControls.forEach((control) => {
            control.addEventListener('change', () => {
                this.propertyChange('_comparisonValues', this.comparisonValuesModel);
            })
        });

        this.kpiValuesControls.forEach((control, index) => {
            control.addEventListener('click', (ctrl) => { this.showComparisonValuesPanel(index) });
        });

        this.shadowDOM.querySelector('#panel-toggle').addEventListener('click', () => {
            this.hideComparisonValuesPanel();
        });

        this.shadowDOM.querySelector('.flip-card-button').addEventListener('click', () => {
            this.flipCard();
        });

        this.shadowDOM.querySelector('.unflip-card-button').addEventListener('click', () => {
            this.unflipCard();
        });

        this.shadowDOM.querySelectorAll('i.switch').forEach((element, index) => {
            this.handleSwitchElement(element, index);
        });
    }

    getKpiValueLabel(index) {
        const label = this.shadowDOM.getElementById('kpi-' + index + '-value');
        if (!label) {
            throw new Exception('Element not found!');
        }

        return label;
    }

    getKpiValueDescription(index) {
        const label = this.shadowDOM.getElementById('kpi-' + index + '-desc');
        if (!label) {
            throw new Exception('Element not found!');
        }

        return label;
    }

    setKpiValue(index, value) {
        const control = this.getKpiValueLabel(index);

        const formatSetting = this.config['widgetNumberFormatting'];
        const scaleSetting = this.config[`kpi${index}ComparisonScaling`];
        const widgetScaling = this.getConfiguration('widgetScaling');

        let scaling = scaleSetting ?? widgetScaling;

        const decimalPlaces = this.config[`kpi${index}ComparisonDecimalPlaces`];

        control.innerText = format(value, formatSetting, decimalPlaces, scaling);

        this._values[index - 1] = value;

        for (let i = 1, j = 4; i <= j; i++) {
            this.updateComparisonValueIndicator(index, i);
        }

    }

    setComparisonValue(kpiIndex, valueIndex, value) {
        if (kpiIndex > 4 || kpiIndex < 1) {
            throw new Exception('Incorrect kpi index provided');
        }

        if (valueIndex > 4 || valueIndex < 1) {
            throw new Exception('Incorrect comparison value index provided');
        }

        const index = ((kpiIndex - 1) * 4) + (valueIndex - 1);

        this._comparisonValuesArray[index] = value;

        const kpi = Array.from(this.shadowDOM.querySelectorAll('.compare-values-container'))[kpiIndex - 1];
        const control = Array.from(kpi.querySelectorAll('span.comp-value'))[valueIndex - 1];

        const kpiScaling = this.getConfiguration(`kpi${kpiIndex}ComparisonScaling`);
        const widgetScaling = this.getConfiguration('widgetScaling');
        const kpiValue = this._values[kpiIndex - 1];

        let scaleSetting = kpiScaling ?? widgetScaling;

        if (scaleSetting === Scaling.Automatic) {
            scaleSetting = getScaling(kpiValue);
        }

        const decimalPlaces = this.getConfiguration(`kpi${kpiIndex}AbsoluteDecimalPlaces`);
        const numberFormat = this.getConfiguration('widgetNumberFormatting');

        const formattedValue = format(value, numberFormat, decimalPlaces, scaleSetting, '', true);

        control.innerText = formattedValue;

        // const isIcon = this.getConfiguration(`kpi${kpiIndex}Trend`) === 'icon';

        // if (isIcon) {
        //     kpi.classList.add('icons');
        // } else {
        //     kpi.classList.remove('icons');
        // }

        this.updateComparisonValueIndicator(kpiIndex, valueIndex);

    }

    setKpiDescription(index, value) {
        const control = this.getKpiValueDescription(index);

        control.value = value.toString();
    }


    createComparisonValuesPanel(index) {
        const panel = this.shadowDOM.querySelectorAll('.kpi')[index];
        const copy = panel.cloneNode(true);

        copy.classList.add('values-panel');



        return copy;
    }

    showComparisonValuesPanel(index) {
        if (this.designMode) return;

        const overlay = this.shadowDOM.querySelector('.overlay');
        const wrapper = this.shadowDOM.querySelector('.kpis-wrapper');
        const toggle = this.shadowDOM.querySelector('#panel-toggle');

        hideElement(wrapper);
        showElement(overlay);
        showElement(toggle);

        const copy = this.createComparisonValuesPanel(index);

        overlay.appendChild(copy)


        this.handleSwitchElement(overlay.querySelector('i.switch'), index);



        overlay.querySelector('i.switch').addEventListener('click', () => {
            this.hideComparisonValuesPanel();

            const element = this.shadowDOM.querySelector(`#kpi-${index + 1}-switch`);

            if (!element.classList.contains('percentage')) {
                element.classList.add('percentage');
                element.classList.remove('absolute');

            } else {
                element.classList.add('absolute');
                element.classList.remove('percentage');

            }

            this.showComparisonValuesPanel(index);
        });
    }

    hideComparisonValuesPanel() {
        const overlay = this.shadowDOM.querySelector('.overlay');
        const wrapper = this.shadowDOM.querySelector('.kpis-wrapper');
        const toggle = this.shadowDOM.querySelector('#panel-toggle');

        hideElement(overlay);
        showElement(wrapper);
        hideElement(toggle);

        overlay.innerHTML = '';
    }

    calculateIconClass(value) {
        if (value > 0) {
            return 'positive';
        }

        if (value < 0) {
            return 'negative';
        }

        return 'unchanged';
    }

    updateComparisonValueIndicator(kpiIndex, valueIndex, deviationDisplay) {

        const index = ((kpiIndex - 1) * 4) + (valueIndex - 1);
        const kpiValue = this._values[kpiIndex - 1];

        const value = this._comparisonValuesArray[index];
        const kpi = Array.from(this.shadowDOM.querySelectorAll('.compare-values-container'))[kpiIndex - 1];
        const icon = Array.from(kpi.querySelectorAll('i.comp-trend'))[valueIndex - 1];
        const label = Array.from(kpi.querySelectorAll('label.comp-label'))[valueIndex - 1];

        const isIcon = this.getConfiguration(`kpi${kpiIndex}Trend`) === 'icon';
        const useColor = this.getConfiguration(`kpi${kpiIndex}TrendThreshold`) === 'traffic';

        const displayMode = deviationDisplay ?? this.getConfiguration(`kpi${kpiIndex}DeviationDisplay`);

        const options = {
            "percentage": {
                suffix: '%',
                value: (val, kpiVal) => calculateChangeInPercentage(val, kpiVal),
                decimalPlaces: () => this.getConfiguration(`kpi${kpiIndex}PercentageDecimalPlaces`),
                format: (valToShow, numberFormat, decimalPlaces, scaling, suffix) => format(valToShow, numberFormat, decimalPlaces, Scaling.Default, suffix, false)
            },
            "absolute": {
                suffix: '',
                value: (val, kpiVal) => calculateChange(val, kpiVal),
                decimalPlaces: () => this.getConfiguration(`kpi${kpiIndex}AbsoluteDecimalPlaces`),
                format: (valToShow, numberFormat, decimalPlaces, scaling, suffix) => format(valToShow, numberFormat, decimalPlaces, scaling, suffix, false)
            }
        }


        const strategy = options[displayMode];

        const valToShow = strategy.value(value, kpiValue);
        const decimalPlaces = strategy.decimalPlaces();
        const suffix = strategy.suffix;

        const kpiScaling = this.getConfiguration(`kpi${kpiIndex}ComparisonScaling`);
        const absoluteScaling = this.getConfiguration(`kpi${kpiIndex}AbsoluteScaling`);
        const widgetScaling = this.getConfiguration('widgetScaling');

        let scaling = absoluteScaling ?? kpiScaling ?? widgetScaling;

        if ((kpiScaling || widgetScaling) && scaling === Scaling.Automatic) {
            scaling = getScaling(this._values[kpiIndex - 1]);
        }

        const numberFormat = this.getConfiguration('widgetNumberFormatting');

        const formattedValue = strategy.format(valToShow, numberFormat, decimalPlaces, scaling, suffix);

        label.innerHTML = formattedValue;


        if (!isIcon) {
            hideElement(icon);
            showElement(label);
        } else {
            hideElement(label);
            showElement(icon);
        }

        icon.classList.remove('positive', 'negative', 'yellow', 'red', 'green');
        label.classList.remove('positive', 'negative', 'yellow', 'red', 'green');

        //    if (useColor) {
        this.applyColoring(options, icon, label, kpiIndex, kpiValue, valueIndex, value);
        //  }
    }



    applyColoring(strategies, icon, label, kpiIndex, kpiValue, valueIndex, value) {
        const colors = ['Green', 'Yellow', 'Red'];
        const ranges = colors.map(color => this.createRange(color, kpiIndex, valueIndex));
        const coloringBase = this.getConfiguration(`kpi${kpiIndex}Cv${valueIndex}Trend`) ?? 'absolute';
        const coloringStrategy = strategies[coloringBase];
        const selectedRange = getRange(ranges, coloringStrategy.value(value, kpiValue));

        const iconSet = this.getConfiguration('iconSet');
        const trend = this.getConfiguration(`kpi${kpiIndex}Trend`) || 'values';
        const trendTreshold = this.getConfiguration(`kpi${kpiIndex}TrendThreshold`) || 'accent';

        if (selectedRange) {
            const selectedColor = colors[ranges.indexOf(selectedRange)];

            if (trend === 'icon') {
                icon.classList.add(iconSet);
            }

            icon.classList.add(selectedColor.toLowerCase());
            label.classList.add(selectedColor.toLowerCase());

            if (trendTreshold === 'accent') {
                icon.classList.add('accent');
                label.classList.add('accent');
            }

        }

    }

    createRange(color, kpiIndex, comparisonValueIndex) {
        const min = this.getConfiguration(`kpi${kpiIndex}Cv${comparisonValueIndex}${color}Min`) ?? -Infinity;
        const max = this.getConfiguration(`kpi${kpiIndex}Cv${comparisonValueIndex}${color}Max`) ?? Infinity;

        return [min, max];
    }

    flipCard() {
        this.rootControl.classList.add('flipped');
    }

    unflipCard() {
        this.rootControl.classList.remove('flipped');
    }

    getConfiguration(key) {
        return this.config[key];
    }

    disableKpis() {
        for (let i = 1, j = 4; i <= j; i++) {
            const key = `kpi${i}Enabled`;
            const value = this.getConfiguration(key);
            this.kpiVisibility(i, value);
        }
    }

    disableComparisonValues() {
        for (let i = 1, j = 4; i <= j; i++) {
            for (let k = 1, l = 4; k <= l; k++) {
                const key = `kpi${i}ComparisonValue${k}Enabled`;
                const value = this.getConfiguration(key);

                this.comparisonValueVisibility(i, k, value);
            }
        }
    }

    comparisonValueVisibility(kpiIndex, valueIndex, show) {
        // 1, 3, 5, 7  .comp-container (2n-1)
        // 2, 4, 6, 8 .comp-value (2n)


        const compValueElementIndex = valueIndex;

        const valueElement = this.shadowDOM.querySelector(`.kpi-${kpiIndex} .compare-values-container > .comp-wrapper:nth-child(${compValueElementIndex})`);

        if (show) {
            showElement(valueElement);
        } else {
            hideElement(valueElement);
        }


    }

    kpiVisibility(index, show) {
        const element = this.shadowDOM.querySelector(`.kpi-${index}`);
        const separator = this.shadowDOM.querySelector(`.kpi-${index} + div.separator`);

        if (show) {
            showElement(element);
            showElement(separator);
        } else {
            hideElement(element);
            hideElement(separator);
        }

    }

    applyConfig(config) {
        this.config = { ...this.config, ...config };


        Object.keys(config).forEach(key => {
            const cssVariableName = `--${key.replace(/[A-Z]/g, m => "-" + m.toLowerCase())}`;

            this.shadowRoot.host.style
                .setProperty(cssVariableName, config[key]);
        });

        this.handleBold('#sac-widget-name', this.getConfiguration('widgetTitleStyleBold'));
        this.handleKpiBold();
        this.handleSwitchVisibilty();

    }

    handleBold(selector, isBold) {
        this.shadowDOM.querySelector(selector).style.fontWeight = isBold ? 'bold' : 'normal';
    }

    handleKpiBold() {
        for (let i = 1, j = 4; i <= j; i++) {
            this.handleBold(`#kpi-${i}-value`, this.getConfiguration(`kpi${i}StyleBold`));
        }
    }

    handleSwitchVisibilty() {
        for (let i = 1, j = 4; i <= j; i++) {
            const isSwitchVisible = this.getConfiguration(`kpi${i}DeviationDisplaySwitch`);
            const swich = this.shadowDOM.querySelector(`#kpi-${i}-switch`);
            const defMode = this.getConfiguration(`kpi${i}DeviationDisplay`) || 'absolute';

            swich.classList.add(defMode);

            if (!isSwitchVisible) {
                hideElement(swich);
            } else {
                showElement(swich);
            }

        }
    }

    switchDeviationMode(kpiIndex, mode) {
        for (let i = 1, j = 4; i < j; i++) {
            this.updateComparisonValueIndicator(kpiIndex, i, mode);
        }
    }

    handleSwitchElement(element, index) {
        element.addEventListener('click', (ev) => {
            if (!element.classList.contains('percentage')) {
                element.classList.add('percentage');
                element.classList.remove('absolute');
                this.switchDeviationMode(index + 1, 'percentage');
            } else {
                element.classList.add('absolute');
                element.classList.remove('percentage');
                this.switchDeviationMode(index + 1, 'absolute');
            }
        });
    }

}

customElements.define("com-widget", WidgetWebComponent);