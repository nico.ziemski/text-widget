import {
    scaleBand,
    scaleLinear
} from 'd3-scale';

import {
    range,
    max,
} from 'd3-array';

export function xScale(data, width) {
    return scaleBand()
        .domain(range(data.length))
        .rangeRound([0, width])
        .paddingInner(0.1);
}

export function xLineScale(data, width) {
    return scaleLinear()
        .domain([0, data.length])
        .range([0, width]);
}

export function yScale(data, height) {
    return scaleLinear()
        .domain([0, max(data)])
        .range([height, 0]);

}