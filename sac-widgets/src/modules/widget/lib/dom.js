

export function hideElement(element) {
    const classes = element.classList;
    element.classList.add('hide');
}

export function showElement(element) {
    const classes = element.classList;
    element.classList.remove('hide');
}

export function setElementReadOnly(element) {
    element.setAttribute('readonly', true);
}

export function removeElementReadOnly(element) {
    element.removeAttribute('readonly');
}

export function resizableInput(el, factor = 7) {
    var int = Number(factor) || 7.7;
    function resize() { el.style.width = ((el.value.length + 1) * int) + 'px' }
    var e = 'keyup,keypress,focus,blur,change'.split(',');
    for (var i in e) el.addEventListener(e[i], resize, false);
    resize();
}

export function adjustInputSize(el, int = 5.5) {
    el.style.width = ((el.value.length + 1) * int) + 'px';
}