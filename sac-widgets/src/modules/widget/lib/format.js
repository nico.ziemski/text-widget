export const NumberFormat = {
    Dot: "0",
    Comma: "1",
    Space: "2"
}

export const Scaling = {
    Default: "0",
    Thousands: "1",
    Million: "2",
    Billion: "3",
    Automatic: "4"

}

export function format(value = 0.00, numberFormat = NumberFormat.Dot, decimalPlaces = 2, scaling = Scaling.Default, suffix = '', useLabel = true) {
    let scaledValue = value;

    const scale = {
        [Scaling.Default]: 1,
        [Scaling.Thousands]: 1000,
        [Scaling.Million]: 1000000,
        [Scaling.Billion]: 1000000000
    };

    const label = {
        [Scaling.Default]: '',
        [Scaling.Thousands]: 't',
        [Scaling.Million]: 'm',
        [Scaling.Billion]: 'b'
    }

    
    if (scaling === Scaling.Automatic) {
        scaling = getScaling(value);
    }

    const labelText = useLabel ? label[scaling] : '';

    const formatter = createFormatter(numberFormat, decimalPlaces);




    scaledValue = value / scale[scaling];

    return `${formatter.format(scaledValue)}${labelText}${suffix}`;
}

export function getScaling(value) {
    const significantDigits = numberOfSignificantDigits(value);

    if (between(significantDigits, 4, 6)) {
        return Scaling.Thousands;
    }

    if (between(significantDigits, 7, 9)) {
        return Scaling.Million;
    }

    if (between(significantDigits, 10, 999)) {
        return Scaling.Billion;
    }

    return Scaling.Default;
}

function between(x, min, max) {
    return x >= min && x <= max;
}

function numberOfSignificantDigits(value) {
    const split = value.toString().split('.');

    return split[0].length;
}

function createFormatter(numberFormat, decimalPlaces) {
    let formattingOptions = {
        useGrouping: true,
        minimumFractionDigits: decimalPlaces ?? 2,
        maximumFractionDigits: decimalPlaces ?? 2
    };
    let localeIdentifier = 'en-US';

    switch (numberFormat) {
        default:
        case NumberFormat.Dot:
            break;
        case NumberFormat.Comma:
            localeIdentifier = 'de-DE';
            break;
        case NumberFormat.Space:
            localeIdentifier = 'fr-FR'
            break;
    }

    return new Intl.NumberFormat(localeIdentifier, formattingOptions);
}

function inRange(range, value) {
    const min = range[0];
    const max = range[1];

    if (value >= min && value < max) {
        return range;
    }

    return undefined;
}

export function getRange(ranges = [], value = 0) {
    for (let i = 0, j = ranges.length; i < j; i++) {
        const range = inRange(ranges[i], value);

        if (range) {
            return range;
        }
    }

    return undefined;
}