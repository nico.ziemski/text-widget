export function calculateChange(value, base) {
    if (value && !isNaN(value) && base && !isNaN(base)) {
        return (value - base);
    } else {
        return 0;
    }
}

export function calculateChangeInPercentage(value, base) {
    const calculated = calculateChange(value, base);
    if (!isNaN(base) && base !== 0) {
        return (calculated / base * 100);
    }
    return 0;
}  