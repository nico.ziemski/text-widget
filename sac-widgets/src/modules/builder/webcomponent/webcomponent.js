import styles from '../scss/style.scss';
import template from '../html/template.html';
import { toCamelCase, collapsablePanel, translate, builderDropDowns, builderCustomCheckboxes } from '../webcomponent/helpers.js';

import { DEFAULT_SETTINGS } from '../../config/defaults';
import { translations } from '../resources/index';
// Import TinyMCE
import tinymce from 'tinymce/tinymce';

// Default icons are required for TinyMCE 5.3 or above
import 'tinymce/icons/default';


const templateDom = document.createElement('template');

templateDom.innerHTML = `
  <style>${styles.toString()}</style>
  ${template.toString()}`;



class WidgetWebComponent extends HTMLElement {

    set _config(value) {
        if (value) {
            try {
                const config = JSON.parse(value);

                this.applyConfig(config);
            }
            catch {
                console.error('invalid json format', value)
            }
        }
    }

    set description(value) {
        this.textarea.innerHTML = value; 
    }

    /*
    You can implement this function to execute JavaScript code when the Web Component is initialized,
    for example, when the custom widget is added to the canvas or the analytic application is opened.
    This function is the counterpart of onCustomComponentDestroy.
    */

    constructor() {
        super();

        this.shadowDOM = this.attachShadow({ mode: 'open' });

        this.createTextArea();

        

        this.shadowDOM.appendChild(templateDom.content.cloneNode(true));

        this._config;
        this.config;

        this.setDefaults();

        builderDropDowns(this.shadowDOM);
        builderCustomCheckboxes(this.shadowDOM);

        this.addEventListeners();

        collapsablePanel(this.shadowDOM);

    }

    /*
    You can implement this function to execute JavaScript code when this Web Component of the custom widget is connected to the HTML DOM of the web page
    */

    connectedCallback() {
        this.initTiny();
    }

    /*
    You can implement this function to execute JavaScript code when this Web Component of the custom widget is disconnected from the HTML DOM of the web page.
    */

    disconnectedCallback() {

    }

    /*
    You can implement this function to execute JavaScript code after the properties of the custom widget have been updated.
    The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
    The key is the name of the property, the value is the changed value of the property.
    When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
    The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
    */

    onCustomWidgetAfterUpdate(oChangedProperties) {

    }

    /*
    You can implement this function to execute JavaScript code before the properties of the custom widget are updated.
    The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
    The key is the name of the property, the value is the changed value of the property.
    When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
    The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
    */

    onCustomWidgetBeforeUpdate(oChangedProperties) {

    }

    /*
    You can implement this function to execute JavaScript code when the custom widget is destroyed,
    for example, when the custom widget is removed from the canvas or the analytic application containing the custom widget is closed.
    This function is the counterpart of constructor().
    */

    onCustomWidgetDestroy() {

    }

    /*
    You can implement this function to execute JavaScript code when the custom widget is resized.
    The argument width is the new width of the custom widget in pixels.
    The argument height is the new height of the custom widget in pixel
    */

    onCustomWidgetResize(width, height) {

    }

    propertyChange(propertyName, value) {
        const event = {
            detail: { properties: {} }
        };

        event.detail.properties[propertyName] = value;

        this.dispatchEvent(new CustomEvent("propertiesChanged", event));

        console.log(event);
    }

    addEventListeners() {
        this.shadowDOM.querySelectorAll('input:not([type=checkbox]):not([type=radio])')
            .forEach(e => {
                e.addEventListener('change', (event) => {
                    const element = event.target;
                    const propName = toCamelCase(element.getAttribute('id'));

                    this.config = { ...this.config, [propName]: event.target.value };
                    this.propertyChange("_config", JSON.stringify(this.config));
                });
            });

        this.shadowDOM.querySelectorAll('input[type="radio"]')
            .forEach(e => {
                e.addEventListener('change', (event) => {
                    const element = event.target;
                    const propName = toCamelCase(element.getAttribute('name'));

                    this.config = { ...this.config, [propName]: event.target.value };
                    this.propertyChange("_config", JSON.stringify(this.config));
                })
            });

        this.shadowDOM.querySelectorAll('input[type="checkbox"]')
            .forEach(e => {
                e.addEventListener('change', (event) => {
                    const element = event.target;
                    const propName = toCamelCase(element.getAttribute('id'));


                    this.config = { ...this.config, [propName]: event.target.checked };
                    this.propertyChange("_config", JSON.stringify(this.config));
                });
                e.addEventListener('click', (event) => {
                    //event.stopPropagation();
                })
            });

        this.shadowDOM.querySelectorAll('select')
            .forEach(e => {
                e.addEventListener('change', (event) => {
                    const element = event.target;
                    const propName = toCamelCase(element.getAttribute('id'));

                    this.config = { ...this.config, [propName]: event.target.value };
                    this.propertyChange("_config", JSON.stringify(this.config));
                });
            });

        this.shadowDOM.querySelectorAll('[data-controls]')
            .forEach(element => {
                element.addEventListener('change', (event) => {
                    const target = event.target.getAttribute('data-controls');
                    this.applyColoring(`.${target}`, event.target.value);
                });
            });

        this.shadowDOM.querySelectorAll('[name="icon-set"]')
            .forEach(element => {
                element.addEventListener('change', (ev) => {
                    for (let i = 1, j = 4; i <= j; i++) {
                        this.handleThreshold(i);
                    }
                    // this.shadowDOM.querySelectorAll('.trend-icon-green, .trend-icon-yellow, .trend-icon-red')
                    //     .forEach(ele => {
                    //         ele.classList.remove('icon-set1', 'icon-set2', 'icon-set3');
                    //         ele.classList.add(ev.target.value);
                    //     })
                })
            })

        for (let i = 1, j = 4; i <= j; i++) {
            this.shadowDOM.querySelectorAll(`[name="kpi${i}-trend"]`)
                .forEach(e => {
                    e.addEventListener('change', (ev) => {
                        this.handleThreshold(i);
                    });
                });

            this.shadowDOM.querySelectorAll(`#kpi${i}-panel .add-icon`)
                .forEach(icon => {
                    icon.addEventListener('click', (ev) => {
                        this.addComparisonValue(i);
                    });
                });

            this.shadowDOM.querySelectorAll(`#kpi${i}-panel .remove-icon`)
                .forEach((icon, index) => {
                    icon.addEventListener('click', (ev) => {
                        this.removeComparisonValue(i, index + 1);
                    });
                });

            this.shadowDOM.querySelectorAll(`[name="kpi${i}-trend-threshold"]`)

                .forEach(e =>
                    e.addEventListener('change', (ev) => {
                        const parent = this.shadowDOM.querySelector(`#kpi${i}-values-panel`)

                        parent.querySelectorAll('.trend-icon-green, .trend-icon-yellow, .trend-icon-red')
                            .forEach(x => {

                                if (ev.target.value === 'accent') {
                                    this.handleKpiAccentColor(i);
                                } else {
                                    this.handleKpiIconColors(i);
                                }
                            })
                    })
                );
        }

        this.shadowDOM.querySelector('#add-kpi-button').addEventListener('click', () => {
            this.addKpi();
        });

        this.shadowDOM.querySelectorAll('.kpi.remove-icon').forEach((element, index) => {
            element.addEventListener('click', () => {
                this.removeKpi(index + 2);
            });
        });

        this.textarea.addEventListener('change', (ev) => {
            this.propertyChange('description', ev.target.value);
        });
    }

    setDefaults() {
        this.applyConfig(DEFAULT_SETTINGS);
        this.translate();
    }

    translate() {
        const language = navigator.language;
        const resources = translations.find(t => t.languages.includes(language));

        if (!resources) {
            console.warn(`No language pack for ${language} specified`);
            return;
        }

        translate(resources.translations, this.shadowDOM);
    }

    applyConfig(config) {

        this.config = { ...this.config, ...config };
        Object.keys(this.config).forEach(key => {
            const id = key.replace(/[A-Z]/g, m => "-" + m.toLowerCase());

            const element = this.shadowDOM.querySelector(`#${id}`);

            if (element) {
                element.value = element.checked = this.config[key];
                //element.checked = this.config[key];
            } else {
                const group = this.shadowDOM.querySelector(`[name="${id}"][value="${this.config[key]}"]`);
                if (group) {
                    group.checked = true;
                }
            }

        });

        this.handleKpisState();
        this.handleIconColors();

        this.handleThresholds();

    }

    applyColoring(iconClass, backgroundColor) {
        const affectedItems = Array.from(this.shadowDOM.querySelectorAll(`${iconClass}`));

        affectedItems.forEach(item => item.style.backgroundColor = backgroundColor);
    }

    handleThresholds() {
        for (let i = 1, j = 4; i <= j; i++) {
            this.handleThreshold(i);
        }
    }

    removeClassFromSelector(selection, ...classNames) {
        selection
            .forEach(element =>
                element.classList.remove(...classNames));
    }

    addClassToSelector(selection, ...classNames) {
        selection
            .forEach(element =>
                element.classList.add(classNames));
    }

    handleKpisState() {
        for (let i = 1, j = 4; i <= j; i++) {
            this.handleKpiState(i);
        }
    }

    handleThreshold(kpiIndex) {
        const isIcon = this.config[`kpi${kpiIndex}Trend`] === 'icon';
        const iconSet = this.config['iconSet'];

        const parent = this.shadowDOM.querySelector(`#kpi${kpiIndex}-values-panel`);
        this.removeClassFromSelector(parent.querySelectorAll('.trend-icon-green, .trend-icon-yellow, .trend-icon-red'), 'icon-set1', 'icon-set2', 'icon-set3');

        if (isIcon) {
            this.addClassToSelector(parent.querySelectorAll('.trend-icon-green, .trend-icon-yellow, .trend-icon-red'), iconSet);
        }

        const usesTrafficLight = this.config[`kpi${kpiIndex}TrendThreshold`] === 'traffic';

        if (usesTrafficLight) {
            this.handleKpiIconColors(kpiIndex);
        } else {
            this.handleKpiAccentColor(kpiIndex);
        }
    }

    handleKpiIconColors(kpiIndex) {
        const green = this.config['trendPlusColor'];
        const yellow = this.config['trendEqualsColor'];
        const red = this.config['trendMinusColor'];

        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-green`, green);
        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-yellow`, yellow);
        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-red`, red);
    }

    handleIconColors() {
        const green = this.config['trendPlusColor'];
        const yellow = this.config['trendEqualsColor'];
        const red = this.config['trendMinusColor'];

        this.applyColoring('.trend-icon-green', green);
        this.applyColoring('.trend-icon-yellow', yellow);
        this.applyColoring('.trend-icon-red', red);
    }

    handleKpiAccentColor(kpiIndex) {
        const color = this.config['trendAccentColor'];
        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-green`, color);
        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-yellow`, color);
        this.applyColoring(`#kpi${kpiIndex}-values-panel .trend-icon-red`, color);

    }

    handleAccentColor() {
        const color = this.config['trendAccentColor'];

        this.applyColoring('.trend-icon-green', color);
        this.applyColoring('.trend-icon-yellow', color);
        this.applyColoring('.trend-icon-red', color);
    }

    handleKpiState(kpiIndex) {
        const kpiWrappr = this.shadowDOM.querySelector(`.kpi${kpiIndex}-wrapper`);

        const isEnabled = this.config[`kpi${kpiIndex}Enabled`];

        if (isEnabled) {
            kpiWrappr.classList.remove('hide');
        } else {
            kpiWrappr.classList.add('hide');
        }

        for (let i = 1, j = 4; i <= j; i++) {
            this.handleComparisonValueState(kpiIndex, i);
        }
    }

    handleComparisonValueState(kpiIndex, comparisonValueIndex) {
        const isEnabled = this.config[`kpi${kpiIndex}ComparisonValue${comparisonValueIndex}Enabled`];
        const wrapper = this.shadowDOM.querySelector(`#kpi${kpiIndex}-values-panel .cv${comparisonValueIndex}-wrapper`);

        if (isEnabled) {
            wrapper.classList.remove('hide');
        } else {
            wrapper.classList.add('hide')
        }
    }

    getLastVisibleKpi() {
        for (let i = 1, j = 4; i <= j; i++) {
            const isEnabled = this.config[`kpi${i}Enabled`];
            if (!isEnabled) {
                return i;
            }
        }

        return -1;
    }

    getLastVisibleComparisonValue(kpiIndex) {
        for (let i = 1, j = 4; i <= j; i++) {
            const isEnabled = this.config[`kpi${kpiIndex}ComparisonValue${i}Enabled`];

            if (!isEnabled) {
                return i;
            }
        }

        return -1;
    }

    removeComparisonValue(kpiIndex, valueIndex) {
        const checkbox = this.shadowDOM.querySelector(`#kpi${kpiIndex}-comparison-value${valueIndex}-enabled`);

        if (checkbox) {
            checkbox.checked = false;
            // const ev = new Event('change', { bubbles: true });
            // checkbox.dispatchEvent(ev);

            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", true, true);
            checkbox.dispatchEvent(evt);
        }

        this.handleKpiState(kpiIndex);
    }

    addComparisonValue(kpiIndex) {
        const valueIndex = this.getLastVisibleComparisonValue(kpiIndex);
        const checkbox = this.shadowDOM.querySelector(`#kpi${kpiIndex}-comparison-value${valueIndex}-enabled`);

        if (checkbox) {
            checkbox.checked = true;
            // const ev = new Event('change', { bubbles: true });
            // checkbox.dispatchEvent(ev);

            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", true, true);
            checkbox.dispatchEvent(evt);
        }
        this.handleKpiState(kpiIndex);
    }

    addKpi() {
        const lastVisibleKpi = this.getLastVisibleKpi();
        const checkbox = this.shadowDOM.querySelector(`#kpi${lastVisibleKpi}-enabled`);

        if (checkbox) {
            checkbox.checked = true;
            // const ev = new Event('change', { bubbles: true });
            // checkbox.dispatchEvent(ev);

            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", true, true);
            checkbox.dispatchEvent(evt);
        }
        this.handleKpiState(lastVisibleKpi);
    }

    removeKpi(kpiIndex) {
        const checkbox = this.shadowDOM.querySelector(`#kpi${kpiIndex}-enabled`);

        if (checkbox) {
            checkbox.checked = false;
            // const ev = new Event('change', { bubbles: true });
            // checkbox.dispatchEvent(ev);

            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", true, true);
            checkbox.dispatchEvent(evt);
        }
        this.handleKpiState(kpiIndex);
    }

    createTextArea() {
        this.textarea = document.createElement('textarea');
        this.appendChild(this.textarea)
    }

    initTiny() {

       this.tiny = tinymce.init({
            target: this.textarea,
            skin: 'oxide',
            setup: (ed) =>  {
                ed.on('change', (e) => {
                    this.propertyChange('description', e.target.getContent());
                });
            }
        });
    }
}

customElements.define("com-builder", WidgetWebComponent);