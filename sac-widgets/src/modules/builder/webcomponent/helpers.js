
var DEFAULT_REGEX = /[-_]+(.)?/g;

function toUpper(match, group1) {
    return group1 ? group1.toUpperCase() : '';
}
export function toCamelCase(str, delimiters) {
    return str.replace(delimiters ? new RegExp('[' + delimiters + ']+(.)?', 'g') : DEFAULT_REGEX, toUpper);
};

export function toggle(element, className = 'collapsed') {
    const classesAsArray = Array.from(element.classList);

    if (classesAsArray.includes(className)) {
        element.classList.remove(className);
    } else {
        element.classList.add(className);
    }
}

export function collapsablePanel(element) {
    const headers = element.querySelectorAll('.collapsable-header');

    headers.forEach(header => {
        const panelId = header.dataset.collapses;
        const panel = element.querySelector(`#${panelId}`);
        const arrow = header.querySelector('.arrow');

        if (!panel) {
            throw `Missing collapsable content ${panelId}`;
        }

        arrow.addEventListener('click', (event) => {
            toggle(panel);
            toggle(header, 'content-collapsed');

            const classList = Array.from(arrow.classList);

            if (classList.includes('down')) {
                arrow.classList.remove('down');
                arrow.classList.add('right');
            } else {
                arrow.classList.add('down');
                arrow.classList.remove('right');
            }

        })
    });



    // if (!header) {
    //     throw new Exception('Missing collapsable header');
    // }



    // header.addEventListener('click', (event) => {
    //     toggle(element);
    // });
}

export function translate(resources, root) {
    Object.keys(resources).forEach(key => {
        const id = key.replace(/[A-Z]/g, m => "-" + m.toLowerCase());
        const elementsAffected = root.querySelectorAll('[i18n="' + id + '"]')
        elementsAffected.forEach(element => element.innerText = resources[key]);
    });
}

export function dropDown(element) {
    const arrow = element.querySelector('.arrow');
    const picker = element.querySelector('.input-color');

    element.addEventListener('click', () => {
        picker.click();
    });
}

export function builderDropDowns(root) {
    const dropdowns = Array.from(root.querySelectorAll('.input-color-wrapper'));

    dropdowns.forEach(drop => dropDown(drop));
}

export function createSwitch(checkboxElement) {
    const sw = document.createElement('div');
    const copy = checkboxElement.cloneNode(true);
    checkboxElement.parentNode.insertBefore(sw, checkboxElement);

    if (checkboxElement.checked) {
        sw.classList.add('checked');
    } else {
        sw.classList.remove('checked');
    }

    if (checkboxElement.classList.length > 0) {
        sw.classList.add(checkboxElement.classList);
    }

    copy.addEventListener('click', (ev) => {
        ev.stopPropagation();
    })

    sw.addEventListener('click', (ev) => {
        copy.click();
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", true, true);
        checkboxElement.dispatchEvent(evt);
        ev.stopPropagation();
    });

    copy.addEventListener('change', (ev) => {
        console.log(ev);
        if (copy.checked) {
            sw.classList.add('checked');
        } else {
            sw.classList.remove('checked');
        }
    });

    copy.classList.add('hide');
    sw.classList.add('custom-checkbox');
    sw.appendChild(copy);
    checkboxElement.parentNode.removeChild(checkboxElement);


}

export function builderCustomCheckboxes(root) {
    const checkboxes = Array.from(root.querySelectorAll('input[type="checkbox"]'));

    checkboxes.forEach(chb => createSwitch(chb));

}