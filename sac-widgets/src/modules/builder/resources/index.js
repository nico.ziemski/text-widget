import { resources as en } from './en';
import { resources as de } from './de';

export const translations = [en, de];