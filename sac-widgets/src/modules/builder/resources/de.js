export const resources = {
    languages: ['de', 'de-DE'],
    translations: {
        absolute: "Absolut",
        accentColor: "Akzent-Farbe",
        advanced: "Erweitert",
        backgroundColor: "Hintergrund",
        barChart: "Balkendiagram",
        barChartColor: "Balkenfarbe",
        billion: "Milliarde",
        borderColor: "Rahmen",
        borderSize: "Rahmengröße",
        comparisonValues: "Vergleichswert",
        decimalPlaces: "Dezimalstellen",
        deviationValueAbsolute: "Abweichung absolut",
        deviationValueDisplay: "Darstellung der Abweichung",
        deviationValuePercentage: "Abweichung relativ",
        diagram: "Diagramm",
        font: "Schriftart",
        fontAccentColor: "Schriftart Akzent-Farbe",
        fontColor: "Schriftfarbe",
        fontSize: "Schriftgröße",
        generalSettings: "Allgemeine Einstellungen",
        hover: "Hover",
        icon: "Icon",
        iconButton: "Icon/Schaltfläche",
        kpiAndComparisonValue: "KPI & Vergleichswert",
        kpis: "KPIs",
        lineChart: "Liniendiagramm",
        lineChartColor: "Linienfarbe",
        million: "Million",
        none: "Unformatiert",
        numberFormatting: "Zahlenformat",
        percentage: "Prozent",
        scaling: "Skalierung",
        style: "Schriftstil",
        switch: "Umschalter",
        thickness: "Konturenstärke",
        thousand: "Tausend",
        title: "Titel",
        trafficLights: "Ampel",
        trend: "Trend",
        trendThreshold: "Trendschwelle",
        values: "Werte"
    }
}