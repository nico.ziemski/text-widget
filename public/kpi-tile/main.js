(function() {
    let template = document.createElement('template');
    template.innerHTML = `
       <style>
           :host {
                text-align: center;
                font-size: 2.5em;
                border-style: solid;
			}
        </style>

        <div class="tile">
            <div>
                <p>Actual Period</p>
            </div>

            <span>
                <p>Previous Period</p>
            </span>

            <span>
                <p>Absolut Deviation</p>
            </span>
        </div>
    `;

    class KPI extends HTMLElement{
        constructor() {
            super();

        let shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.appendChild(template.content.cloneNode(true));
        
        this._props = {};
        };

        onCustomWidgetBeforeUpdate(changedProperties) {
			this._props = { ...this._props, ...changedProperties };
		}

		onCustomWidgetAfterUpdate(changedProperties) {
			if ("backgroundcolor" in changedProperties) {
				this.style["background-color"] = changedProperties["backgroundcolor"];
			}
            if ("bordercolor" in changedProperties) {
				this.style["border-color"] = changedProperties["bordercolor"];
			}
            if ("borderwidth" in changedProperties) {
				this.style["border-width"] = changedProperties["borderwidth"];
            }
		}

    }

    customElements.define("kpi-tile", KPI);

})();
