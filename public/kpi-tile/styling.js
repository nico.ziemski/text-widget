(function()  {
    let template = document.createElement("template");
    template.innerHTML = `
        <style>
			.appearance {
				background-color: #00929f;
				color: white;
				padding: 5px;
			}
		</style>
		<form id="form" class="appearance">
            <fieldset>
                <legend>Styling Properties</legend>
                <table>
                    <tr>
                        <td>Background color</td>
                        <td><input id="bg_color" type="text" size="30" maxlength="15"></td>
                    </tr>
                    <tr>
                        <td>Border Color</td>
                        <td><input id="border_color" type="text" size="30" maxlength="15"></td>
                    </tr>
                    <tr>
                        <td>Border Width</td>
                        <td><input id="border_width" type="text" size="30" maxlength="6"></td>
                    </tr
                </table>
                <input type="submit" style="display:none;">
            </fieldset>
        </form>
    `;

    class KPIStyling extends HTMLElement {
        constructor(){
            super();
            this._shadowRoot = this.attachShadow({mode: "open"});
            this._shadowRoot.appendChild(template.content.cloneNode(true));
            this._shadowRoot.getElementById("form").addEventListener("submit", this._submit.bind(this));
        }

        _submit(e){
            e.preventDefault();
            this.dispatchEvent(new CustomEvent("propertiesChanged", {
                        detail: {
                            properties: {
                                backgroundcolor: this.backgroundcolor,
                                bordercolor: this.bordercolor,
                                borderwidth: this.borderwidth
                            }
                        }
            }));
        }

        set backgroundcolor(newBackgroundColor) {
            this._shadowRoot.getElementById("bg_color").value= newBackgroundColor;
        }

        set bordercolor(newBorderColor) {
            this._shadowRoot.getElementById("border_color").value= newBorderColor;
        }

        set borderwidth(newBorderWidth) {
            this._shadowRoot.getElementById("border_width").value= newBorderWidth;
        }

        get backgroundcolor(){
            return this._shadowRoot.getElementById("bg_color").value;
        }

        get bordercolor(){
            return this._shadowRoot.getElementById("border_color").value;
        }
        
        get borderwidth(){
            return this._shadowRoot.getElementById("border_width").value;
        }
    }

    customElements.define("kpi-tile-styling", KPIStyling); 
})();
