(function () {
    class TileBuilderWebComponent extends HTMLElement {
        constructor() {
            super();
        }



        /*
        You can implement this function to execute JavaScript code when this Web Component of the custom widget is connected to the HTML DOM of the web page
        */

        connectedCallback() {

        }

        /*
        You can implement this function to execute JavaScript code when this Web Component of the custom widget is disconnected from the HTML DOM of the web page.
        */

        disconnectedCallback() {

        }

        /*
        You can implement this function to execute JavaScript code after the properties of the custom widget have been updated.
        The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
        The key is the name of the property, the value is the changed value of the property.
        When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
        The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
        */

        onCustomWidgetAfterUpdate(oChangedProperties) {

        }

        /*
        You can implement this function to execute JavaScript code before the properties of the custom widget are updated.
        The argument oChangedProperties is a JavaScript object containing the changed properties as key-value pairs.
        The key is the name of the property, the value is the changed value of the property.
        When this function is called for the very first time by the Custom Widget SDK framework, the full list of properties is passed in oChangedProperties.
        The property value of a property is the default value if defined in the custom widget JSON or undefined otherwise.
        */

        onCustomWidgetBeforeUpdate(oChangedProperties) {

        }

        /*
        You can implement this function to execute JavaScript code when the custom widget is destroyed,
        for example, when the custom widget is removed from the canvas or the analytic application containing the custom widget is closed.
        This function is the counterpart of constructor().
        */

        onCustomWidgetDestroy() {

        }

        /*
        You can implement this function to execute JavaScript code when the custom widget is resized.
        The argument width is the new width of the custom widget in pixels.
        The argument height is the new height of the custom widget in pixel
        */

        onCustomWidgetResize(width, height) {

        }

        propertyChange(propertyName, value) {
            const event = {
                detail: { properties: {} }
            };

            event.detail.properties[propertyName] = value;

            this.dispatchEvent(new CustomEvent("propertiesChanged", event));
        }
    }

    customElements.define("com-next-tile-builder", TileBuilderWebComponent);
})();