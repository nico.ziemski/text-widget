(function()  {
    let template = document.createElement("template");
    template.innerHTML = `
        <style>
			.appearance {
				color: #000;
				padding: 5px;
            }
            
            textarea {
                border: solid 1px #ccc;
                width: 100%;
                height: 100px;
            }
		</style>
		<form id="form" class="appearance">
            <fieldset>
                <legend>Styling Properties</legend>
                <table>
                    <tr>
                        <td>You can set your custom css in the textarea below</td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="areaCss"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    `;

    class TileStyling extends HTMLElement {
        constructor(){
            super();
            this._shadowRoot = this.attachShadow({mode: "open"});
            this._shadowRoot.appendChild(template.content.cloneNode(true));
            this._shadowRoot.getElementById("form").addEventListener("submit", this._submit.bind(this));
            this._shadowRoot.getElementById('areaCss').addEventListener('blur', this._submit.bind(this));
        }

        get css() {
            return this._shadowRoot.getElementById('areaCss').value;
        }

        set css(value) {
            this._shadowRoot.getElementById('areaCss').value = value;
        }

        _submit(e){
            e.preventDefault();
            this.dispatchEvent(new CustomEvent("propertiesChanged", {
                        detail: {
                            properties: {
                                css: this.css
                            }
                        }
            }));
        }
    }

    customElements.define("next-tile-styling", TileStyling); 
})();