(function () {

    const d3Script = document.createElement('script');
    d3Script.src = 'https://d3js.org/d3.v5.min.js';
    d3Script.async = false;
    document.head.appendChild(d3Script);



    const fontRoboto = document.createElement('link');
    fontRoboto.href = '//fonts.googleapis.com/css?family=Roboto&display=swap';
    fontRoboto.rel = 'stylesheet';

    document.head.appendChild(fontRoboto);


    const template = document.createElement('template');
    template.innerHTML = `
        <style>
        *, html {
            font-family: 'Roboto';
          }
          
          .container {
          }
          
          :host {
              width: 274px;
              height: 350px;            
          }
          
          .tile {
              font-size: 1em;
              width: 274px;
              height: 350px;
              padding-top: 6px;
              background: #FFFFFF00 0% 0% no-repeat;
              border: 0.5px solid #BCBEC0;
              border-radius: 4px;
              box-sizing: border-box;   
              padding-left: 13px;
              padding-right: 13px;
          }
          
          .title {
              /* Layout Properties */
              display: inline-block;
              /* UI Properties */
              text-align: left;
              font-size: 30px;
              letter-spacing: 0;
              color: #000000;
              margin: 0;
              margin-bottom: 2px;
              border: 0;
              border-bottom: solid 0.5px #00929F;
              outline: none;
              resize: none;
              width: 200px;
              overflow: hidden;
              text-overflow: ellipsis;
          }

          .title[readonly] {
            pointer-events: none;            
            user-select: none;
          }

          .title::placeholder, .title::-webkit-input-placeholder {
              opacity: 0.3;
          }

          .title[readonly]::placeholder, .title[readonly]::-webkit-input-placeholder {
              opacity: 0;
          }
          
          label {
              width: 100%;
              display: block;
              font-size: 10px;
              color: #BCBEC0;
              margin-top: 16px;
          }
          
          .value {
              font-size: 30px;
              font-weight: bold;
              line-height: 30px;
              margin-right: 20px;
          }
          
          rect {
              fill: #00929F;
          }
          
          .small {
              text-align: left;
              font-size: 15px;
              letter-spacing: 0;
              line-height: 15px;
          }
          
          .negative {                
              color: #EF4135;
          }
          
          .positive {
              color: #00929F;                 
          }
          
          .positive:before {
              content: '+';
          }
          
          .toggle {
              width: 41px;
              height: 12px;
              border: solid 1px #ccc;
              cursor: pointer;
              box-sizing: border-box;
          }
          
          td, tr {
              padding: 0;
              margin: 0;
          }
          
          .toggle > div {
              font-size: 10px;
              line-height: 10px;
              padding: 0;
              margin: 0;
              text-align: center;
              float: left;
              width: 50%;
              height: 10px;
          }
          
          .toggle > div.left {               
              background-color: #FFFFFF;
              color: #BCBEC0;
          }
          
          .toggle > div.right {
              background-color: #BCBEC0;
              color: #ffffff;
          }
          
          .flip-card {
              background-color: transparent;
              width: 100%;
              height: 300px;
              perspective: 1000px; /* Remove this if you don't want the 3D effect */
            }
            
            /* This container is needed to position the front and back side */
            .flip-card-inner {
              position: relative;
              width: 100%;
              height: 100%;   
              transition: transform 0.8s;
              transform-style: preserve-3d;
            }
            
            /* Do an horizontal flip when you move the mouse over the flip box container */
            .flip-card.flipped .flip-card-inner {
              transform: rotateY(180deg);
            }
            
            /* Position the front and back side */
            .flip-card-front, .flip-card-back {
              position: absolute;
              width: 100%;
              height: 100%;
              -webkit-backface-visibility: hidden; /* Safari */
              backface-visibility: hidden;
            }
            
            /* Style the front side (fallback if image is missing) */
            .flip-card-front {
              
            }
            
            /* Style the back side */
            .flip-card-back {
              padding-top: 10px;
              text-align: left;
              font-size: 12px;
              color: #BCBEC0;
              box-sizing: border-box;
              overflow: auto;
              transform: rotateY(180deg);
            }
          
            #details {
              border: solid 1px #ccc;
              border-radius: 30px;
              display: block;
              float: right;
              height: 18px;
              width: 18px;
              margin-top: 0px;
              cursor: pointer;
            }
          
            #details > i {
              font-size: 16px;
              line-height: 18px;
              text-align: center;
              display: block;
              font-style: normal;
            }

            #tileSvg {
                margin-top: 16px;
            }
              
                  
        </style>
        <div class="tile" id="tile">
        <input type="text" placeholder="Set title.." class="title" id="title">
        <label id="details">
              <i>i</i>
        </label>
        <div class="flip-card" id="flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front" id="content">
                <table>
                <tr colspan="2">
                    <td><label class="description" id="kpi-desc-1">Actual (in Mio.)</label></td>
                </tr>
                <tr>
                    <td rowSpan="2">
                        <span class="value" id="kpi-val-1">0.00</span>            
                    </td>
                    <td>
                        <span class="deviation positive small" id="kpi-target-1">0.00</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="toggle" id="top-kpi-toggle">
                            <div class="left">%</div>
                            <div class="right">abs</div>
                        </div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td>
                        <label class="description" id="kpi-desc-2">YtD (in Mio.)</label>
                    </td>
                </tr>
            
            <tr>
                <td rowSpan="2">
                <span class="value" id="kpi-val-2">0.00</span>           
                </td>
                <td>
                <span class="deviation positive small" id="kpi-target-2">0.00</span>
                </td>
            </tr>
            <tr>
                <td>
                <div class="toggle" id="bottom-kpi-toggle">
                            <div class="left">%</div>
                            <div class="right">abs</div>
                        </div>
                </td>
            </tr>
        </table>         
                </div>
                <div class="flip-card-back" id="description">
                </div>
            </div>
        </div>            
            
        </div>
    `;

    const CHART_HEIGHT = 144;

    d3Script.onload = () => {
        customElements.define('next-tile',


            class Tile extends HTMLElement {
                constructor() {
                    super();

                    if (!window._d3) {
                        window._d3 = d3;
                    }

                    this.props = {};
                    this.width = 248;
                    this.height;

                    this.topDisplayMode;
                    this.bottomDisplayMode;

                    this.shr = this.attachShadow({ mode: 'open' });
                    this.styleNode = document.createElement('style');
                    this.shr.appendChild(template.content.cloneNode(true));
                    // this.shr.appendChild(d3Script);

                    const topKpiToggle = this.shr.getElementById('top-kpi-toggle');
                    const bottomKpiToggle = this.shr.getElementById('bottom-kpi-toggle');

                    topKpiToggle.addEventListener('click', () => this.toggleTop());
                    bottomKpiToggle.addEventListener('click', () => this.toggleBottom());
                    this.shr.getElementById('details').addEventListener('click', () => this.flip());

                    this.titleControl.addEventListener('change', (value) => {
                        this.propagateValueChange('title', value.target.value);
                    });

                    this.topBase = 0.00;
                    this.topValue = 0.00;
                    this.topDeviation = true;

                    this.bottomValue = 0.00;
                    this.bottomBase = 0.00;
                    this.bottomDeviation = true;
                    this.designMode = false;

                    this.addEventListener("click", event => {
                        var event = new Event("onClick");
                        this.dispatchEvent(event);
                    });


                    this.createSvgElement();
                }

                //public exposed props
                set topValue(value) {
                    this.topKPIValue = value;
                    this.shr.getElementById('kpi-val-1').innerText = !isNaN(value) ? value.toFixed(2) : '';
                    this.populateTopTargetText();
                }

                set topBase(value) {
                    this.topKPIBase = value;
                    this.populateTopTargetText();
                }

                set topDescription(value) {
                    if (typeof value !== 'undefined') {
                        this.shr.getElementById('kpi-desc-1').innerText = value;
                    }
                }

                set bottomValue(value) {
                    this.bottomKPIValue = value;
                    this.shr.getElementById('kpi-val-2').innerText = !isNaN(value) ? value.toFixed(2) : '';
                    this.populateBottomTargetText();
                }

                set bottomBase(value) {
                    this.bottomKPIBase = value;
                    this.populateBottomTargetText();
                }

                set bottomDescription(value) {
                    if (typeof value !== 'undefined') {
                        this.shr.getElementById('kpi-desc-2').innerText = value;
                    }
                }

                set data(value) {
                    
                    if (typeof value !== 'undefined') {
                        this._data = value;
                        this.createChart();
                    }
                }

                set description(value) {
                    if (typeof value !== 'undefined') {
                        this.shr.getElementById('description').innerText = value;
                    }
                }

                set css(value) {
                    if (typeof value !== 'undefined') {
                        this.styleNode.innerHTML = value;
                        this.shr.appendChild(this.styleNode.cloneNode(true));
                    }
                }

                set title(value) {
                    if (typeof value !== 'undefined') {
                        this.shr.getElementById('title').value = value;
                    }
                }

                set topDeviation(value) {
                    if (typeof value === 'boolean') {
                        this.topDisplayMode = value;
                        this.populateTopTargetText();
                        this.populateTopToggleClasses(value);
                    } else {
                        console.warn(`${value} is not boolean`);
                    }
                }

                get topDeviation() {
                    return this.topDisplayMode;
                }

                set bottomDeviation(value) {
                    if (typeof value === 'boolean') {
                        this.bottomDisplayMode = value;
                        this.populateBottomTargetText();
                        this.populateBottomToggleClasses(value);
                    } else {
                        console.warn(`${value} is not boolean`);
                    }
                }

                get bottomDeviation() {
                    return this.bottomDisplayMode;
                }

                get titleControl() {
                    return this.shr.getElementById('title');
                }

                set designMode(value) {
                    if (value) {
                        this.titleControl.removeAttribute('readonly');
                    } else {
                        this.titleControl.setAttribute('readonly', true);
                    }
                }

                //end of public exposed props

                populateTopTargetText() {
                    const value = this.calculateChange(this.topKPIValue, this.topKPIBase);
                    let text = value.toFixed(2);

                    if (this.topDisplayMode) {
                        const percentage = this.calculateChangeInPercentage(this.topKPIValue, this.topKPIBase);
                        text = this.createDeviationText(percentage, '%');
                    }

                    const targetLabel = this.shr.getElementById('kpi-target-1');
                    targetLabel.innerText = text;
                    targetLabel.className = ['deviation', 'positive', 'negative', 'small']
                        .filter(e => (value >= 0 && e !== 'negative') || (value < 0 && e !== 'positive')).join(' ');
                }

                populateBottomTargetText() {
                    const value = this.calculateChange(this.bottomKPIValue, this.bottomKPIBase);
                    let text = value.toFixed(2);

                    if (this.bottomDisplayMode) {
                        const percentage = this.calculateChangeInPercentage(this.bottomKPIValue, this.bottomKPIBase);
                        text = this.createDeviationText(percentage, '%');
                    }

                    const targetLabel = this.shr.getElementById('kpi-target-2');
                    targetLabel.innerText = text;
                    targetLabel.className = ['deviation', 'positive', 'negative', 'small']
                        .filter(e => (value >= 0 && e !== 'negative') || (value < 0 && e !== 'positive')).join(' ');
                }

                populateTopToggleClasses = (value) => {
                    const left = this.shr.querySelector("#top-kpi-toggle div:first-child");
                    const right = this.shr.querySelector("#top-kpi-toggle div:last-child");

                    if (value) {
                        left.className = 'right';
                        right.className = 'left';
                    } else {
                        left.className = 'left';
                        right.className = 'right';
                    }
                };

                populateBottomToggleClasses = (value) => {
                    const left = this.shr.querySelector("#bottom-kpi-toggle div:first-child");
                    const right = this.shr.querySelector("#bottom-kpi-toggle div:last-child");

                    if (value) {
                        left.className = 'right';
                        right.className = 'left';
                    } else {
                        left.className = 'left';
                        right.className = 'right';
                    }
                };

                createDeviationText(value, pattern) {
                    return `${value.toFixed(2)}${pattern}`;
                }

                createChart() {

                    
                    //create x scale;
                    var xScale = window._d3.scaleBand()
                        .domain(window._d3.range(this._data.length))
                        .rangeRound([0, this.width])
                        .paddingInner(0.1);

                    // create y scale
                    var yScale = window._d3.scaleLinear()
                        .domain([0, window._d3.max(this._data)])
                        .range([CHART_HEIGHT, 0]);

                    // create bars                
                    this.svg.selectAll("rect")
                        .data(this._data)
                        .enter()
                        .append("rect")
                        .attr("x", function (d, i) {
                            return xScale(i);
                        })
                        .attr("y", function (d) {
                            return yScale(0);
                        })
                        .attr("width", xScale.bandwidth())
                        .attr("height", function (d) {
                            return CHART_HEIGHT - yScale(0);
                        });

                    // animate bars
                    this.svg.selectAll("rect")
                        .transition()
                        .duration(800)
                        .attr("y", function (d) { return yScale(d) })
                        .attr("height", function (d) { return CHART_HEIGHT - yScale(d); })
                        .delay(function (d, i) { return (i * 100) })
                }

                calculateChange(value, base) {
                    if (value && !isNaN(value) && base && !isNaN(base)) {
                        return (value - base);
                    } else {
                        return 0;
                    }
                }

                calculateChangeInPercentage(value, base) {
                    const calculated = this.calculateChange(value, base);
                    if (!isNaN(base) && base !== 0) {
                        return (calculated / base * 100);
                    }
                    return 0;
                }

                onCustomWidgetDestroy() {
                    try {
                        document.head.removeChild(d3Script);
                    }
                    catch{ }
                }

                disconnectedCallback() {
                    // your cleanup code goes here

                }

                connectedCallback() {
                    // this.recreateSvg();
                }

                onCustomWidgetResize(width, height) {
                    this.resizeSvgElement();
                }

                resizeSvgElement(width) {
                    
                    this.width = width;

                    this.removeSvgElement();
                    this.createSvgElement();
                    this.createChart();


                }

                onCustomWidgetBeforeUpdate(changedProperties) {
                    this.props = { ...this.props, ...changedProperties };
                    this.designMode = changedProperties.hasOwnProperty('designMode') ? changedProperties.designMode : this.designMode;
                }

                onCustomWidgetAfterUpdate(changedProperties) {

                }

                removeSvgElement() {
                    if (this.svg) {
                        window._d3.select(this.shr.getElementById('content')).select("svg").remove();
                    }
                }

                createSvgElement() {
                 
                    this.svg = window._d3.select(this.shr.getElementById('content'))
                        .append("svg")
                        .attr("id", "tileSvg")
                        .attr("width", this.width)
                        .attr("height", CHART_HEIGHT);
                }

                toggleTop() {
                    const topDevMode = this.topDeviation;
                    this.topDeviation = !topDevMode;
                }

                toggleBottom() {
                    const bottomDevMode = this.bottomDeviation;
                    this.bottomDeviation = !bottomDevMode;
                }

                flip() {
                    const flipCard = this.shr.getElementById('flip-card');
                    const classes = flipCard.className.split(' ');
                    const isFlipped = classes.indexOf('flipped') > -1;

                    if (isFlipped) {
                        flipCard.className = classes.filter(name => name !== 'flipped');
                    } else {
                        flipCard.className = classes.concat('flipped').join(' ');
                    }
                }

                propagateValueChange(propertyName, value) {
                    const event = {
                        detail: { properties: {} }
                    };

                    event.detail.properties[propertyName] = value;

                    this.dispatchEvent(new CustomEvent("propertiesChanged", event));
                }
            }
        )
    }
})();